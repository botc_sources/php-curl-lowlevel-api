<?php
namespace BotCLL;


use BotCLL\Core\ApiRequest;
use BotCLL\Core\Credentials;
use BotCLL\Core\JsonRequester;
use BotCLL\Core\SignerV1;

abstract class BaseRequester {

    /**
     * @return string
     */
    abstract public function getUrl();

    /**
     * @return Credentials
     */
    abstract public function getCredentials();

    /**
     * @param $function
     * @param $urlParams
     * @param $method
     * @param $params
     * @param $headers
     * @return mixed
     */
    protected function call($function,$urlParams,$method,$params,$headers){
        $plainRequest = new ApiRequest(
            $this->getUrl(),
            $function,
            $urlParams,
            $method,
            $params,
            $headers
        );
        $signer = new SignerV1($this->getCredentials());
        $signedRequest = $signer->sign($plainRequest);
        $requester = new JsonRequester();
        return $requester->send($signedRequest);
    }

}
