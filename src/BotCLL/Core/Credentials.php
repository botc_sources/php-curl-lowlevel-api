<?php
namespace BotCLL\Core;


interface Credentials{
    public function getPublicId();
    public function getSecret();
}