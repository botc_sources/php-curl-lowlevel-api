<?php
namespace BotCLL\Core;

interface Requester{
    public function send(Request $request);
}