<?php
namespace BotCLL\Core;

interface Signer{
  public function sign(Request $request);
}
